README for the Chemical inventory module for Drupal 5.x

This module is customized for managing an inventory of chemicals for a
small laboratory.  It defines a new node type "chemical", and a page that
presents a listing of the chemical inventory.

Each chemical also gets a sequential, unique ID number for recording usage
in a lab notebook.  Chemicals that are used up can be edited and marked as
"empty" so that they no longer appear except in the list of "empty"
chemicals. This is intended for later reference back from lab notebooks to
a specific chemical used in an experiment where the lot number, supplier,
etc. may be important information for comparision between experimental 
results.

This module defines 3 user permissions:

add new chemicals	
administer chemical inventory	
update chemicals

Only a user with the "administer" permssion can change the module settings.
Only a user with both the "update" and "administer" permissions can delete
a chemical once it's added to the inventory.

written by Peter Wolanin (pwolanin@drupal)
